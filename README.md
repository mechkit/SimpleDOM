# SimpleDOM

An experimental view layer.

## API

    import $ from 'specdom';

    $(input, specs)
    // or
    $(specs)

if input starts with '#', the rest if the string is used to select the element with the matching id. Starting with '.' selects the class. Pass an DOM element, and it will be wrapped.

The returned object is a wrapped DOM element with the methods listed below. The class selection returns an Array of wrapped elements.

If specs are passed, a new DOM element is created based on the given specifications. If specs is a String, a textNode is created.

## Structure

| key                   | alt. key | content                  |
| :-------------------- | :------- | :----------------------- |
| tag                   | _        | HTML tag                 |
| children              | _c       | Array of child specs     |
| text                  | _t       | Text content for element |
| meta                  | _m       |                          |
| additional_properties | _a       |                          |
| additional_properties | _p       |                          |

Everything else gets put under "props";

## Example

```JSON
{
  _ 'div',
  class: 'title'
  _c [
    {
      _ 'span',
      _t 'hello world'
    },
    {
      _ 'span',
      _c [
        'goodbye world'
      ]
    }
  ]
}
```
becomes:

```JSON
{
  tag: 'div',
  props: {
    class: 'title'
  },
  children: [
    {
      tag: 'span',
      text: 'hello world'
    },
    {
      tag: 'span',
      children: [
        'goodbye world'
      ]
    }
  ]
}
```

becomes:

```html
<div class="title">
  <span>hello world</span>
  <span>goodbye world</span>
</div>
```

Note: The raw HTML is not normally generated, it is included for illistration. SpecDOM directly alters the page's DOM.

Though you can create a page with the raw specs, it is often easier to use something like [SpecDOM_helper](https://gitlab.com/mechkit/specdom_helper).


## Methods

### text(string)

Sets the [textContent](https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent) of the node.

### append(input)

Appends given element(s) to this element.

### appendTo(parentElement)

Appends this element to given element.

### attr(name, value)

Sets or removes an element's atrubute.

### css(name, value)

Sets CSS/style variable name to value. If no value is given, it returns the element's value.

### unwrap()

Returns the wrapped DOM element.

### clear()

Removes any children.

### load(new_specs)

Loads the given specs into this SpecDOM object, replacing the existing element.

### parent()

Returns this elements parrent, still wrapped by SpecDOM.

### value(new_value)

Sets or gets this elements value. Useful for select and input elements.
